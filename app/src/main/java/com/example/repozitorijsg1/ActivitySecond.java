package com.example.repozitorijsg1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.content.SharedPreferences;
import android.widget.Toast;

public class ActivitySecond extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String SavedText = "txtKey";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Button btnAct1 = findViewById(R.id.btnAct1);

        btnAct1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent( ActivitySecond.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

       Button btnRead =findViewById(R.id.btnRead);
       final TextView resView = findViewById(R.id.resView);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        btnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String retrievedText;
                if (sharedpreferences.contains(SavedText)) {
                    retrievedText = sharedpreferences.getString(SavedText, "");
                }
                else {
                    retrievedText = "didn't find any";
                }

                resView.setText(retrievedText, null);
                Toast.makeText(ActivitySecond.this,retrievedText, Toast.LENGTH_LONG).show();
            }
        });


    }
}
