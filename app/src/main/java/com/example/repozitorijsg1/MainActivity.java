package com.example.repozitorijsg1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String SavedText = "txtKey";
    SharedPreferences sharedpreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnAct2 = findViewById(R.id.btnAct2);
        btnAct2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent( MainActivity.this, ActivitySecond.class);
                startActivity(intent);
            }
        });

        Button btnSave = findViewById(R.id.btnSave);
        final EditText txtEntry =findViewById(R.id.txtEntry);




        sharedpreferences = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        btnSave.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //Intent intent = new Intent( MainActivity.this, ActivitySecond.class);
                //startActivity(intent);
                String txtToSave  = txtEntry.getText().toString();
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(SavedText, txtToSave);
                //editor.apply();
                editor.commit();
                String retrievedText;
                if (sharedpreferences.contains(SavedText)) {
                    retrievedText = sharedpreferences.getString(SavedText, "");
                }
                else {
                    retrievedText = "didn't find any";
                }
                Toast.makeText(MainActivity.this,retrievedText, Toast.LENGTH_LONG).show();
            }
        });
    }
}
